package model;

import javax.ws.rs.Path;
import javax.xml.bind.annotation.XmlRootElement;

//@Path("Post")
@XmlRootElement
public class Post {
	private String text;
	private int id;
	
	public Post(){}
	
	public Post(String text, int id){
		this.text = text;
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public String getText() {
		return text;
	}
}
