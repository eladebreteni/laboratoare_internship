package repository;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.Post;



@Path("Repository")
public class Repository {

	List<Post> list = new ArrayList<Post>();
	
	//@POST
	public List<Post> addPosts() {
		list.add(new Post("Yey", 1));
		list.add(new Post("Uf", 2));
		return list;
	}
	
	@GET
	@Path("findById")
	@Produces(MediaType.APPLICATION_XML)
	public Post findById(int id) {
		addPosts();
		for (Post p : list) {
			if (id == p.getId()) {
				return p;
			}
		}
		return null;
	}
	
	@GET
	@Path("findAllPosts")
	@Produces(MediaType.APPLICATION_XML)
	public List<Post> findAllPosts() {
		System.out.println(addPosts().get(0));
		return addPosts();
	}
}
