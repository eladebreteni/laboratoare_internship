import {Component, Input, OnInit} from '@angular/core';

@Component ({
  selector: 'app-generic-component-parent',
  templateUrl: './generic-component-parent.component.html',
  styleUrls: ['./generic-component-parent.component.scss']
})
export class GenericComponentParentComponent implements OnInit {
  @Input() data: string;
  constructor() { }

  ngOnInit() {
    console.log(this.data);
  }

}
